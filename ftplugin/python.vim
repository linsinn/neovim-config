let b:did_ftplugin = 1

setlocal tabstop=4
setlocal softtabstop=-1
setlocal shiftwidth=4
setlocal autoindent
setlocal noexpandtab

" Check Python files with flake8 and pylint.
let b:ale_linters = ['flake8']
" Fix Python files with autopep8 and yapf.
let b:ale_fixers = ['autopep8']
" Disable warnings about trailing whitespace for Python files.
let b:ale_warn_about_trailing_whitespace = 0
